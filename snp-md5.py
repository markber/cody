###########################################
############## only for test ##############
###########################################


import hashlib
import time
from itertools import product
import string


#MyPWD -> 1177c46a5bf82754deaeb9f10905da5f
#PWD -> 0d35c1f17675a8a2bf3caaacd59a65de

strmd5 = "0d35c1f17675a8a2bf3caaacd59a65de"
count=0
start = time.time()

abc = list(string.ascii_lowercase)
ABC = list(string.ascii_uppercase)
abcABC = abc + ABC

for roll in product(abcABC, repeat = 3):
  plainpwd = ''.join(str(i) for i in roll)
  count = count+1
  plain2md5 = hashlib.md5(plainpwd.encode())
  plain2md5 = plain2md5.hexdigest()
  print("Round: {} -> Plaintext: {}".format(count, plainpwd))
  if strmd5 == plain2md5:
      done = time.time()
      elapsed = done - start
      print("Yippie-Ya-Yeah found password: {} md5 hash: {} count: {} elapsed time: {}".format(plainpwd, plain2md5, count, elapsed))
      exit()
